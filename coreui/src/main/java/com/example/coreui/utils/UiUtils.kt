package com.example.coreui.utils

import android.content.res.ColorStateList
import android.gesture.GestureOverlayView
import android.graphics.Paint
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.widget.ImageViewCompat
import androidx.navigation.NavOptions
import androidx.navigation.navOptions
import androidx.viewpager2.widget.ViewPager2
import com.example.coreui.R
import kotlin.math.abs

fun TextView.crossOutText() {
    this.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
}

fun ImageView.setPic(imageResId: Int) {
    this.setImageResource(imageResId)
}

fun ImageView.setTint(@ColorRes colorRes: Int) {
    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(ContextCompat.getColor(context, colorRes)))
}

fun TextView.setMyTextColor(colorRes: Int) {
    this.setTextColor(ContextCompat.getColor(context, colorRes))
}

fun ImageView.setSrcColor(stringRes: Int) {
    this.setColorFilter(
        ContextCompat.getColor(context, stringRes),
        android.graphics.PorterDuff.Mode.SRC_IN
    )
}

fun ImageView.setBackgroundTintColor(stringRes: Int) {
    this.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, stringRes))
}

fun ViewPager2.setOutstateMidPage() {
    val pageMarginPx = 10
    val offsetPx = 30
    this.apply {
        clipToPadding = false
        clipChildren = false
        offscreenPageLimit = 3
        setPageTransformer { page, position ->
            val offset = position * -(2 * offsetPx + pageMarginPx)
            if (this.orientation == GestureOverlayView.ORIENTATION_HORIZONTAL) {
                if (ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                    page.translationX = -offset
                } else {
                    page.translationX = offset
                }
            } else {
                page.translationY = offset
            }
            page.scaleY = 1 - (0.25f * abs(position))
        }
    }
}

fun returnNavOptions(optionChosen: Int): NavOptions {
    val options1 = navOptions {
        anim {
            enter =
                R.anim.slide_in_right
            exit = R.anim.slide_out_left
            popEnter = R.anim.slide_in_left
            popExit = R.anim.slide_out_right
        }
    }
    val options2 = navOptions {
        anim {
            enter =
                R.anim.slide_in_left
            exit = R.anim.slide_out_right
            popEnter = R.anim.slide_in_right
            popExit = R.anim.slide_out_left
        }
    }
    return if (optionChosen == 1) options1 else options2
}




