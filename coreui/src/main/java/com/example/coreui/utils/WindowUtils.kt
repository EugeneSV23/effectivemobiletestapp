package com.example.coreui.utils

import android.os.Build
import android.view.*
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.updatePadding

fun setInsets(vararg v: View, topInsets: Int = 0, botInsets: Int = 0) {
    for (view in v) {
        view.updatePadding(top = topInsets)
        view.updatePadding(bottom = botInsets)
    }
}

fun changeColorOfStatusBar(
    window: Window,
    fragmentNumber: Int,
    statusBarColor: Int? = null,
    navBarColor: Int? = null,
    v: View? = null
) {
    when (fragmentNumber) {
        1 -> {
            // HOME FMT
            window.clearFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (statusBarColor != null && navBarColor != null) {
//                mySetFullScreen(window, statusBarColor, navBarColor)
            }
        }
        2 -> {
            // LIFESET FMT
            v?.let { showSystemUI(window, it) }
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor?.let { window.statusBarColor = it }
            navBarColor?.let { window.navigationBarColor = it }
        }
        3 -> {
            // GENERAL SET FMT
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
    }
}

private fun showSystemUI(window: Window, v: View) {
    WindowCompat.setDecorFitsSystemWindows(window, false)
    WindowInsetsControllerCompat(
        window, v
    ).show(WindowInsetsCompat.Type.systemBars())
}

fun mySetFullScreen(
    window: Window
//    statusBarColor: Int,
//    navBarColor: Int
) {
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//        window.setDecorFitsSystemWindows(false)
//        val controller = window.insetsController
//        if (controller != null) {
//            controller.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
//            controller.systemBarsBehavior =
//                WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//                controller.systemBarsBehavior =
//                    WindowInsetsController.BEHAVIOR_DEFAULT
//            }
//        }
//    } else {
        window.apply {
//            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//            setStatusBarColor(statusBarColor)
//            navigationBarColor = navBarColor
        }
//    }
}

