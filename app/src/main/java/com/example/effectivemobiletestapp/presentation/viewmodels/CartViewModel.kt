package com.example.effectivemobiletestapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.effectivemobiletestapp.domain.di.IoDispatcher
import com.example.effectivemobiletestapp.domain.interactor.BasketInfoInteractor
import com.example.effectivemobiletestapp.domain.model.Basket
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val basketInfoInteractor: BasketInfoInteractor
) : ViewModel() {

    val basketInfoFullforUi = basketInfoInteractor.basketInfoFullforUi

    val basketItemsListForUi = basketInfoInteractor.basketItemsLDforUI

    private var currentNumberOfPhones = 0

    fun increaseNumberOfPhones(basket: Basket) {
        currentNumberOfPhones = basket.numberOfPhones
        viewModelScope.launch(ioDispatcher) {
            basketInfoInteractor.changeNumberOfPhonesInDB(currentNumberOfPhones + 1, basket.id)
                delay(1)
                basketInfoInteractor.updatePrice(
                    (basket.price * (currentNumberOfPhones + 1)), basket.id
                )
        }
    }

    fun decreaseNumberOfPhones(basket: Basket) {
        currentNumberOfPhones = basket.numberOfPhones
        if (basket.numberOfPhones > 0) {
            viewModelScope.launch(ioDispatcher) {
                basketInfoInteractor.changeNumberOfPhonesInDB(currentNumberOfPhones - 1, basket.id)
                delay(1)
                basketInfoInteractor.updatePrice(
                    (basket.price * (currentNumberOfPhones - 1)), basket.id
                )
            }
        }
    }

    fun deletePhoneInBasket(basket: Basket) {
        viewModelScope.launch(ioDispatcher) {
            basketInfoInteractor.deleteBasketItem(basket.id)
        }
    }
}