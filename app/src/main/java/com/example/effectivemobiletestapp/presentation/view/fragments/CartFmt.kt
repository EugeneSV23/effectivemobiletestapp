package com.example.effectivemobiletestapp.presentation.view.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.coreui.LinearMarginItemDecor
import com.example.coreui.utils.addSystemBottomPadding
import com.example.coreui.utils.addSystemTopPadding
import com.example.coreui.utils.returnNavOptions
import com.example.effectivemobiletestapp.R
import com.example.effectivemobiletestapp.databinding.FmtCartBinding
import com.example.effectivemobiletestapp.domain.model.Basket
import com.example.effectivemobiletestapp.presentation.adapters.DefaultAdapter
import com.example.effectivemobiletestapp.presentation.adapters.getBasketRvDelegate
import com.example.effectivemobiletestapp.presentation.viewmodels.CartViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CartFmt : Fragment(R.layout.fmt_cart) {
    private val binding by viewBinding(FmtCartBinding::bind)

    private lateinit var basketRvAdapter: DefaultAdapter<Basket>
    private val viewModel by viewModels<CartViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.addSystemTopPadding()
        binding.contentRoot.addSystemBottomPadding()
        initVars()
        observeLD()
        setBasketRv()
        setOnClickListeners()
    }

    private fun setOnClickListeners() {
        binding.cartIvBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun observeLD() {
        viewModel.basketInfoFullforUi.observe(viewLifecycleOwner) {
            binding.tvDelivery.text = it.delivery
        }

        viewModel.basketItemsListForUi.observe(viewLifecycleOwner) {
            basketRvAdapter.updateItems(it)
            binding.tvTotalSum.text = getTotalPrice(it).toString()
            checkout(it)
        }
    }

    private fun initVars() {
        basketRvAdapter = DefaultAdapter(getBasketRvDelegate(
            {
                viewModel.increaseNumberOfPhones(it)
            }, {
                viewModel.decreaseNumberOfPhones(it)
            }, {
                viewModel.deletePhoneInBasket(it)
            }
        ))
    }

    private fun getTotalPrice(list: List<Basket>): Int {
        var totalPrice = 0
        for (basketItem in list) {
            totalPrice += basketItem.totalPrice
        }
        return totalPrice
    }

    private fun checkout(list: List<Basket>) {
        binding.buttonCheckout.setOnClickListener {
            if (getTotalPrice(list) != 0) {
                Toast.makeText(requireContext(), "Successfully purchased! total sum paid: ${
                    getTotalPrice(list)
                }", Toast.LENGTH_SHORT).show()
                navigateToHome()
            } else {
                Toast.makeText(requireContext(), "Nothing selected!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setBasketRv() {
        with(binding) {
            rvCart.addItemDecoration(LinearMarginItemDecor(16, LinearLayoutManager.VERTICAL))
            rvCart.adapter = basketRvAdapter
        }
    }

    private fun navigateToHome() {
        findNavController().navigate(R.id.homeFmt, null, returnNavOptions(1))
    }
}
