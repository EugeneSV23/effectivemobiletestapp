package com.example.effectivemobiletestapp.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.effectivemobiletestapp.R
import com.example.effectivemobiletestapp.presentation.view.fragments.ItemDetailsFragment
import com.example.effectivemobiletestapp.presentation.view.fragments.ItemFeaturesFragment
import com.example.effectivemobiletestapp.presentation.view.fragments.ItemShopsFragment

class ItemAdapter (private val fragment: Fragment)  : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = ITEM_COUNT

    override fun createFragment(position: Int): Fragment =
        when(position) {
            0 -> ItemShopsFragment()
            1 -> ItemDetailsFragment()
            else -> ItemFeaturesFragment()
        }

    fun getTabTitle(position: Int): String {
        val titleRes =    when(position) {
            0 -> R.string.shops
            1 -> R.string.details
            else -> R.string.features
        }
        return fragment.getString(titleRes)
    }

    companion object {
        private const val ITEM_COUNT = 3
    }


}