package com.example.effectivemobiletestapp.presentation.adapters

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class DefaultAdapter<T>(vararg delegate: AdapterDelegate<List<T>>) :
    ListDelegationAdapter<List<T>>() {

    init {
        delegate.forEach { this.delegatesManager.addDelegate(it) }
    }

    fun updateItems(items: List<T>) {
        setItems(items)
        notifyDataSetChanged()
    }
}