package com.example.effectivemobiletestapp.presentation.view.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.coreui.utils.addSystemTopPadding
import com.example.coreui.utils.returnNavOptions
import com.example.coreui.utils.setOutstateMidPage
import com.example.coreui.utils.setTint
import com.example.effectivemobiletestapp.R
import com.example.effectivemobiletestapp.databinding.FmtProductDetailsContainerBinding
import com.example.effectivemobiletestapp.domain.model.PhoneDetImages
import com.example.effectivemobiletestapp.presentation.adapters.DefaultAdapter
import com.example.effectivemobiletestapp.presentation.adapters.ItemAdapter
import com.example.effectivemobiletestapp.presentation.adapters.getPhoneDetailsImagesDelegate
import com.example.effectivemobiletestapp.presentation.viewmodels.DetailViewModel
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailsContainerFmt : Fragment(R.layout.fmt_product_details_container) {
    private val binding by viewBinding(FmtProductDetailsContainerBinding::bind)
    private lateinit var phoneDetailImagesAdapter: DefaultAdapter<PhoneDetImages>
    private val viewModel by viewModels<DetailViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.addSystemTopPadding()
        phoneDetailImagesAdapter = DefaultAdapter(getPhoneDetailsImagesDelegate())
        val phoneDetailAdapter = ItemAdapter(this)

        with(binding) {
            tabsPager.adapter = phoneDetailAdapter
            TabLayoutMediator(phoneDetailsTabs, tabsPager) { tab, position ->
                tab.text = phoneDetailAdapter.getTabTitle(position)
            }.attach()
            phoneDetailsPagerImages.adapter = phoneDetailImagesAdapter
            phoneDetailsPagerImages.setOutstateMidPage()

            phoneDetBasketIv.setOnClickListener {
                navigateToCart()
            }
            phoneDetIvBack.setOnClickListener {
                navigateToHome()
            }
            phoneDetailsIvFavorite.setOnClickListener {
                arguments?.let {
                    viewModel.setFavoriteItem(
                        ProductDetailsContainerFmtArgs.fromBundle(
                            it
                        ).id
                    )
                }
            }
        }
        viewModel.phoneDetailForUi.observe(viewLifecycleOwner) {
            it?.let {
                with(binding) {
                    if (it.isFavorites) phoneDetailsIvFavorite.setTint(com.example.coreui.R.color.pallette_1)
                    else phoneDetailsIvFavorite.setTint(com.example.coreui.R.color.white)
                    ratingBar.rating = it.rating
                    phoneDetailsTvLabel.text = it.title
                }
            }
        }
        viewModel.phoneDetImagesForUi.observe(viewLifecycleOwner) {
            phoneDetailImagesAdapter.updateItems(it)
        }
    }

    private fun navigateToCart() {
        findNavController().navigate(R.id.cartFmt, null, returnNavOptions(1))
    }

    private fun navigateToHome() {
        findNavController().navigate(R.id.homeFmt, null, returnNavOptions(2))
    }
}


