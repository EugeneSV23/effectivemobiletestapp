package com.example.effectivemobiletestapp.presentation.view.fragments

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.coreui.GridMarginItemDecoration
import com.example.coreui.LinearMarginItemDecor
import com.example.coreui.ZoomInOutPageTrans
import com.example.coreui.utils.addSystemBottomPadding
import com.example.coreui.utils.addSystemTopPadding
import com.example.coreui.utils.returnNavOptions
import com.example.effectivemobiletestapp.R
import com.example.effectivemobiletestapp.databinding.FmtHomeBinding
import com.example.effectivemobiletestapp.domain.model.BestSellerPhone
import com.example.effectivemobiletestapp.domain.model.HomePageTopMenu
import com.example.effectivemobiletestapp.domain.model.HotSalesPhone
import com.example.effectivemobiletestapp.presentation.adapters.DefaultAdapter
import com.example.effectivemobiletestapp.presentation.adapters.getBestSellerPhoneDelegate
import com.example.effectivemobiletestapp.presentation.adapters.getHomePageTopMenuDelegate
import com.example.effectivemobiletestapp.presentation.adapters.getHotSalesPhoneDelegate
import com.example.effectivemobiletestapp.presentation.viewmodels.HomeViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFmt : Fragment(R.layout.fmt_home) {
    private val binding by viewBinding(FmtHomeBinding::bind)
    private lateinit var bestSellerAdapter: DefaultAdapter<BestSellerPhone>
    private lateinit var hotSalesAdapter: DefaultAdapter<HotSalesPhone>
    private lateinit var homePageTopMenuAdapter: DefaultAdapter<HomePageTopMenu>
    private val viewModel by viewModels<HomeViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.addSystemTopPadding()

        binding.run {
            root.addSystemBottomPadding()
            fab.setOnClickListener {
                navigateToCart()
            }
            bottomAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.favorites -> {
                        true
                    }
                    R.id.account -> {
                        true
                    }
                    else -> false
                }
            }
            openFilter.setOnClickListener {
                onClickOpenBottomSheet()
            }
        }
        initAdapters()
        viewModel.homePageMenuList.observe(viewLifecycleOwner) {
            homePageTopMenuAdapter.updateItems(it)
        }
        viewModel.hotSalesListForUi.observe(viewLifecycleOwner) {
            hotSalesAdapter.updateItems(it)
        }
        viewModel.bestSellerListForUi.observe(viewLifecycleOwner) {
            bestSellerAdapter.updateItems(it)
        }
        viewModel.loadDataErr.observe(viewLifecycleOwner) { error ->
            if (!error.isNullOrBlank())
                Toast.makeText(requireContext(), "Error: $error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initAdapters() {
        bestSellerAdapter = DefaultAdapter(getBestSellerPhoneDelegate({
            viewModel.setFavoriteItem(it)
        }, {
            navigateToDetails()
        }))
        hotSalesAdapter = DefaultAdapter(getHotSalesPhoneDelegate {
            viewModel.setIsBuyHotSales(it)
            val text = if (it.isBuy) "${it.title} purchased!" else "${it.title} not available yet!"
            Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
        })
        homePageTopMenuAdapter = DefaultAdapter(getHomePageTopMenuDelegate {
            viewModel.setIsCheckedForMenuItem(it)
        })
        binding.run {
            rvBestSeller.adapter = bestSellerAdapter
            rvBestSeller.addItemDecoration(
                GridMarginItemDecoration(16, 2, GridLayoutManager.VERTICAL)
            )

            rvCategories.adapter = homePageTopMenuAdapter
            rvCategories.addItemDecoration(LinearMarginItemDecor(16, LinearLayoutManager.HORIZONTAL))

            hotSalesPager.adapter = hotSalesAdapter
            hotSalesPager.setPageTransformer(ZoomInOutPageTrans())
            hotSalesPager.currentItem = 1

        }
    }

    private fun onClickOpenBottomSheet() {
        val bottomSheetDialog =
            BottomSheetDialog(requireContext(), com.example.coreui.R.style.AppTheme_BottomSheet)
        bottomSheetDialog.setContentView(R.layout.fmt_home_bot_search)
        val btnDone = bottomSheetDialog.findViewById<TextView>(R.id.button_done)
        val btnCancel = bottomSheetDialog.findViewById<ImageView>(R.id.iv_cancel)
        btnDone?.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        btnCancel?.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        if (!bottomSheetDialog.isShowing) bottomSheetDialog.show()
    }

    private fun navigateToCart() {
        findNavController().navigate(R.id.cartFmt, null, returnNavOptions(2))
    }

    private fun navigateToDetails() {
        val action: NavDirections = HomeFmtDirections.actionHomeFmtToDetailFmt("3")
        findNavController().navigate(action, returnNavOptions(1))
    }

}