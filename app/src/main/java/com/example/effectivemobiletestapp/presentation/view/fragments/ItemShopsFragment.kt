package com.example.effectivemobiletestapp.presentation.view.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.coreui.utils.returnNavOptions
import com.example.effectivemobiletestapp.R
import com.example.effectivemobiletestapp.databinding.FmtItemShopsBinding
import com.example.effectivemobiletestapp.domain.model.PhoneDetail
import com.example.effectivemobiletestapp.presentation.viewmodels.ItemShopsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ItemShopsFragment: Fragment(R.layout.fmt_item_shops) {

    private val viewModel by viewModels<ItemShopsViewModel>()
    private val binding by viewBinding(FmtItemShopsBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.phoneDetailforUi.observe(viewLifecycleOwner) {
            it?.let { updateView(it) }
        }
    }

    private fun updateView(phoneDetail: PhoneDetail){
        binding.run {
            phoneDetTvCpu.text = phoneDetail.cpu
            phoneDetTvCamera.text = phoneDetail.camera
            phoneDetIvMemoryVarOne.text = String.format(
                resources.getString(com.example.coreui.R.string.memory_template),
                phoneDetail.capacity[0]
            )
            phoneDetIvMemoryVarTwo.text = String.format(
                resources.getString(com.example.coreui.R.string.memory_template),
                phoneDetail.capacity[1]
            )
            phoneDetIvColorVarOne.setBackgroundColor(Color.parseColor(phoneDetail.color[0]))
            phoneDetIvColorVarTwo.setBackgroundColor(Color.parseColor(phoneDetail.color[1]))
            phoneDetButtonAddToCart.text = phoneDetail.price
            phoneDetTvRam.text = phoneDetail.sd
            phoneDetTvMemory.text = phoneDetail.ssd
            phoneDetButtonAddToCart.setOnClickListener {
                navigateToCart()
            }
        }
    }

    private fun navigateToCart() {
        findNavController().navigate(R.id.cartFmt, null, returnNavOptions(1))
    }


}

@AndroidEntryPoint
class ItemDetailsFragment: Fragment(R.layout.fmt_item_details) {

}

@AndroidEntryPoint
class ItemFeaturesFragment: Fragment(R.layout.fmt_item_features) {

}