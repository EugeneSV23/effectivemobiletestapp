package com.example.effectivemobiletestapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import com.example.effectivemobiletestapp.domain.interactor.PhoneDetailInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class ItemShopsViewModel  @Inject constructor(
    phoneDetailInteractor: PhoneDetailInteractor
) : ViewModel() {

    val phoneDetailforUi = phoneDetailInteractor.phoneDetailforUi
}