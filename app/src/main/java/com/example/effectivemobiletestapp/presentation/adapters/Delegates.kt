package com.example.effectivemobiletestapp.presentation.adapters

import android.graphics.Color
import android.view.View
import com.example.coreui.utils.*
import com.example.effectivemobiletestapp.databinding.*
import com.example.effectivemobiletestapp.domain.model.*
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.squareup.picasso.Picasso

inline fun getBestSellerPhoneDelegate(
    crossinline onClick: (BestSellerPhone) -> Unit,
    crossinline navigateOnClick: () -> Unit
): AdapterDelegate<List<BestSellerPhone>> =
    adapterDelegateViewBinding({ layoutInflater, root ->
        ItemBestSellerRvBinding.inflate(layoutInflater, root, false)
    }) {
        bind {
            with(binding) {
                Picasso.get().load(item.picture)
                    .placeholder(com.example.coreui.R.drawable.ic_placeholder_image).into(bigIv)
                modelName.text = item.title
                sumFull.crossOutText()
                sumFull.text = item.priceWithoutDiscount
                sumWithDiscount.text = item.discountPrice
                favorite.setOnClickListener {
                    onClick(item)
                }
                bigIv.setOnClickListener {
                    navigateOnClick()
                }
                if (item.isFavorites)
                    favorite.setPic(com.example.coreui.R.drawable.ic_favorite_bot_rv_vert_checked)
                else favorite.setPic(
                    com.example.coreui.R.drawable.ic_favorite_bot_rv_vert_unchecked

                )
            }
        }
    }

inline fun getHomePageTopMenuDelegate(
    crossinline onClick: (HomePageTopMenu) -> Unit
): AdapterDelegate<List<HomePageTopMenu>> =
    adapterDelegateViewBinding({ layoutInflater, root ->
        ItemMenuHomeBinding.inflate(layoutInflater, root, false)
    }) {
        bind {
            with(binding) {
                categoryTitle.text = item.title
                categoryIcon.setPic(item.picture)
                categoryIcon.setOnClickListener {
                    onClick(item)
                }
                categoryIcon.isSelected = item.isSelected
            }
        }
    }

inline fun getHotSalesPhoneDelegate(
    crossinline onClick: (HotSalesPhone) -> Unit
): AdapterDelegate<List<HotSalesPhone>> =
    adapterDelegateViewBinding({ layoutInflater, root ->
        ItemHotSalesPagerBinding.inflate(layoutInflater, root, false)
    }) {

        bind {
            with(binding) {
                Picasso.get().load(item.picture)
                    .placeholder(com.example.coreui.R.drawable.ic_placeholder_image)
                    .into(hotSalesBackgroundBigIv)
                tvHotSalesTitle.text = item.title
                tvHotSalesSubtitle.text = item.subtitle
                buttonHotSalesBuyNow.setOnClickListener {
                    onClick(item)
                }
                tvHotSalesNew.visibility = if (item.isBuy)
                    View.VISIBLE else View.INVISIBLE
            }
        }
    }


fun getPhoneDetailsImagesDelegate(): AdapterDelegate<List<PhoneDetImages>> =
    adapterDelegateViewBinding({ layoutInflater, root ->
        ItemPagerDetailsImagesBinding.inflate(layoutInflater, root, false)
    }) {
        bind {
            with(binding) {
                Picasso.get().load(item.image)
                    .placeholder(com.example.coreui.R.drawable.ic_placeholder_image)
                    .into(phoneDetImagesBackgroundBigIv)
            }
        }
    }


inline fun getBasketRvDelegate(
    crossinline increase: (Basket) -> Unit,
    crossinline decrease: (Basket) -> Unit,
    crossinline delete: (Basket) -> Unit
): AdapterDelegate<List<Basket>> =
    adapterDelegateViewBinding({ layoutInflater, root ->
        RvItemCartBinding.inflate(layoutInflater, root, false)
    }) {

        bind {
            with(binding) {
                Picasso.get().load(item.images)
                    .placeholder(com.example.coreui.R.drawable.ic_placeholder_image)
                    .into(ivPhone)
                tvPhonePrice.text = String.format(
                    context.resources.getString(com.example.coreui.R.string.cart_rv_price),
                    (item.totalPrice).toString()
                )
                tvNumberOfPhones.text = item.numberOfPhones.toString()
                tvPhoneName.text = item.title
                ivIncrease.setOnClickListener {
                    increase(item)
                }
                ivDecrease.setOnClickListener {
                    decrease(item)
                }
                ivDelete.setOnClickListener {
                    delete(item)
                }
            }
        }
    }






