package com.example.effectivemobiletestapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.effectivemobiletestapp.domain.di.IoDispatcher
import com.example.effectivemobiletestapp.domain.interactor.PhoneDetailInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val phoneDetailInteractor: PhoneDetailInteractor
) : ViewModel() {

    val phoneDetailForUi = phoneDetailInteractor.phoneDetailforUi

    val phoneDetImagesForUi = phoneDetailInteractor.phoneDetImages

    fun setFavoriteItem(id: String) {
        viewModelScope.launch(ioDispatcher) {
            val item = phoneDetailInteractor.getPhoneDetailFromDB(id)
            if (item != null) {
                phoneDetailInteractor.setIsFavoritePhoneDet(!item.isFavorites, item.id)
            }
        }
    }
}