package com.example.effectivemobiletestapp.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.effectivemobiletestapp.domain.di.IoDispatcher
import com.example.effectivemobiletestapp.domain.di.MainDispatcher
import com.example.effectivemobiletestapp.domain.interactor.BasketInfoInteractor
import com.example.effectivemobiletestapp.domain.interactor.HomePhoneInteractor
import com.example.effectivemobiletestapp.domain.interactor.PhoneDetailInteractor
import com.example.effectivemobiletestapp.domain.model.BestSellerPhone
import com.example.effectivemobiletestapp.domain.model.HomePageTopMenu
import com.example.effectivemobiletestapp.domain.model.HotSalesPhone
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class HomeViewModel @Inject constructor(
    @MainDispatcher private val mainDispatcher: CoroutineDispatcher,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val homePhoneInteractor: HomePhoneInteractor,
    private val phoneDetailInteractor: PhoneDetailInteractor,
    private val basketInfoInteractor: BasketInfoInteractor
) : ViewModel() {

    val loadDataErr = MutableLiveData<String?>()
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }

    val bestSellerListForUi = homePhoneInteractor.bestSellerListForUi

    fun setFavoriteItem(item: BestSellerPhone) {
        viewModelScope.launch(ioDispatcher) {
            homePhoneInteractor.setIsFavoriteBestSeller(!item.isFavorites, item.id)
        }
    }

    val homePageMenuList = homePhoneInteractor.homePageMenuList

    fun setIsCheckedForMenuItem(item: HomePageTopMenu) {
            homePhoneInteractor.setIsCheckedForMenuItem(item)
    }

    val hotSalesListForUi = homePhoneInteractor.hotSalesListForUi

    fun setIsBuyHotSales(item: HotSalesPhone) {
        viewModelScope.launch(ioDispatcher) {
            if (item.isBuy) {
                homePhoneInteractor.setIsBuy(!item.isBuy, item.id)
            }
        }
    }

    private fun loadAllDataAndInsertInDB() {
        viewModelScope.launch(exceptionHandler + ioDispatcher) {
            homePhoneInteractor.loadAndInsertHotSalesListinDB()
            homePhoneInteractor.loadAndInsertBestSellerListinDB()
            phoneDetailInteractor.loadAndInsertphoneDetailinDB()
            basketInfoInteractor.loadAndInsertBasketInfoFullinDB()
            basketInfoInteractor.loadAndInsertBasketItemsinDB()

            loadDataErr.postValue("")
        }
    }

    private fun onError(message: String) {
        viewModelScope.launch(mainDispatcher) {
            loadDataErr.value = message
        }
    }

    init {
        loadAllDataAndInsertInDB()
    }
}