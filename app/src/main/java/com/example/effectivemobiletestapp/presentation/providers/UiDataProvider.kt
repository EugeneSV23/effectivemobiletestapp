package com.example.effectivemobiletestapp.presentation.providers

import androidx.lifecycle.MutableLiveData
import com.example.coreui.R
import com.example.effectivemobiletestapp.domain.model.HomePageTopMenu

object UiDataProvider {

    val homeTopMenuItems = MutableLiveData(
        listOf(
            HomePageTopMenu(0, R.drawable.ic_small_rv_hor_phones, true, "Phones"),
            HomePageTopMenu(1, R.drawable.ic_small_rv_hor_computers, false, "Computer"),
            HomePageTopMenu(2, R.drawable.ic_small_rv_hor_health, false, "Health"),
            HomePageTopMenu(3, R.drawable.ic_small_rv_hor_books, false, "Books"),
            HomePageTopMenu(4, R.drawable.ic_small_rv_hor_phones, false, "Placeholder"),
            HomePageTopMenu(5, R.drawable.ic_small_rv_hor_phones, false, "Placeholder")
        )
    )

    fun setHomeTopMenuItems(
        homePageTopMenu: List<HomePageTopMenu>,
        target: MutableLiveData<List<HomePageTopMenu>>
    ) {
        target.value = homePageTopMenu
    }
}