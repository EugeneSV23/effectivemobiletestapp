package com.example.effectivemobiletestapp.data.mapper

import android.content.Context
import com.example.effectivemobiletestapp.data.db.model.*
import com.example.effectivemobiletestapp.data.model.BasketInfoFullDTO
import com.example.effectivemobiletestapp.data.model.BestSellerPhoneDTO
import com.example.effectivemobiletestapp.data.model.HotSalesPhoneDTO
import com.example.effectivemobiletestapp.data.model.PhoneDetailDTO
import com.example.effectivemobiletestapp.domain.model.*
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.*
import javax.inject.Inject

class Mapper @Inject constructor(@ApplicationContext private val appContext: Context) {

    private fun mapBestSellerPhoneDTOtoDBE(bestSellerPhoneDTO: BestSellerPhoneDTO) =
        BestSellerPhoneDBE().apply {
            this.id = bestSellerPhoneDTO.id
            this.isFavorites = bestSellerPhoneDTO.isFavorites
            this.title = bestSellerPhoneDTO.title
            this.priceWithoutDiscount = bestSellerPhoneDTO.priceWithoutDiscount
            this.discountPrice = bestSellerPhoneDTO.discountPrice
            this.picture = bestSellerPhoneDTO.picture
        }

    fun mapBestSellerPhoneDTOListtoDB(list: List<BestSellerPhoneDTO>) =
        list.map {
            mapBestSellerPhoneDTOtoDBE(it)
        }

    private fun mapBestSellerPhoneDBEtoObject(bestSellerPhoneDBE: BestSellerPhoneDBE) =
        BestSellerPhone().apply {
            this.id = bestSellerPhoneDBE.id
            this.isFavorites = bestSellerPhoneDBE.isFavorites
            this.title = bestSellerPhoneDBE.title
            this.priceWithoutDiscount = String.format(
                appContext.resources.getString(com.example.coreui.R.string.best_seller_rv_price),
                bestSellerPhoneDBE.priceWithoutDiscount
            )
            this.discountPrice = String.format(
                appContext.resources.getString(com.example.coreui.R.string.best_seller_rv_price),
                bestSellerPhoneDBE.discountPrice
            )
            this.picture = bestSellerPhoneDBE.picture
        }

    fun mapBestSellerPhoneDBEListtoListofObjects(list: List<BestSellerPhoneDBE>) =
        list.map {
            mapBestSellerPhoneDBEtoObject(it)
        }

    private fun mapHotSalesPhoneDTOtoDBE(hotSalesPhoneDTO: HotSalesPhoneDTO) =
        HotSalesPhoneDBE().apply {
            this.id = hotSalesPhoneDTO.id
            this.isNew = hotSalesPhoneDTO.isNew
            this.title = hotSalesPhoneDTO.title
            this.subtitle = hotSalesPhoneDTO.subtitle
            this.picture = hotSalesPhoneDTO.picture
            this.isBuy = hotSalesPhoneDTO.isBuy
        }


    fun mapHotSalesPhoneDTOlisttoDB(list: List<HotSalesPhoneDTO>) =
        list.map {
            mapHotSalesPhoneDTOtoDBE(it)
        }

    private fun mapHotSalesPhoneDBEtoObject(hotSalesPhoneDBE: HotSalesPhoneDBE) =
        HotSalesPhone().apply {
            this.id = hotSalesPhoneDBE.id
            this.isNew = hotSalesPhoneDBE.isNew
            this.title = hotSalesPhoneDBE.title
            this.subtitle = hotSalesPhoneDBE.subtitle
            this.picture = hotSalesPhoneDBE.picture
            this.isBuy = hotSalesPhoneDBE.isBuy
        }

    fun mapHotSalesPhoneDBEListtoListofObjects(list: List<HotSalesPhoneDBE>) =
        list.map {
            mapHotSalesPhoneDBEtoObject(it)
        }

    fun mapPhoneDetailDTOtoDBE(phoneDetailDTO: PhoneDetailDTO) =
        PhoneDetailDBE().apply {
            val colorString = StringJoiner(",")
            for (color in phoneDetailDTO.color) {
                colorString.add(color)
            }
            val capacityString = StringJoiner(",")
            for (capacity in phoneDetailDTO.capacity) {
                capacityString.add(capacity)
            }
            val imagesString = StringJoiner(",")
            for (image in phoneDetailDTO.images) {
                imagesString.add(image)
            }
            this.cpu = phoneDetailDTO.cpu
            this.camera = phoneDetailDTO.camera
            this.capacity = capacityString.toString()
            this.color = colorString.toString()
            this.id = phoneDetailDTO.id
            this.images = imagesString.toString()
            this.isFavorites = phoneDetailDTO.isFavorites
            this.price = phoneDetailDTO.price
            this.rating = phoneDetailDTO.rating
            this.sd = phoneDetailDTO.sd
            this.ssd = phoneDetailDTO.ssd
            this.title = phoneDetailDTO.title
        }

    fun mapPhoneDetailDBEtoObject(phoneDetailDBE: PhoneDetailDBE?) =
        if (phoneDetailDBE == null) null
        else PhoneDetail().apply {
            this.cpu = phoneDetailDBE.cpu
            this.camera = phoneDetailDBE.camera
            this.capacity = phoneDetailDBE.capacity.split(",")
            this.color = phoneDetailDBE.color.split(",")
            this.id = phoneDetailDBE.id
            this.images = phoneDetailDBE.images.split(",")
            this.isFavorites = phoneDetailDBE.isFavorites
            this.price = String.format(
                appContext.resources.getString(com.example.coreui.R.string.add_to_cart_template),
                phoneDetailDBE.price.toString()
            )
            this.rating = phoneDetailDBE.rating
            this.sd = phoneDetailDBE.sd
            this.ssd = phoneDetailDBE.ssd
            this.title = phoneDetailDBE.title
        }

    fun mapPhoneDetImagesFromPhoneDetObj(phoneDetail: PhoneDetail?): List<PhoneDetImages> {
        val result = mutableListOf<PhoneDetImages>()
        if (phoneDetail != null) {
            for (imageInList in phoneDetail.images) {
                val resultElement = PhoneDetImages()
                resultElement.image = imageInList
                result.add(resultElement)
            }
        }
        return result
    }

    fun mapBasketInfoFullDTOtoDBE(basketInfoFullDTO: BasketInfoFullDTO) =
        BasketInfoFullDBE().apply {
            val listOfIds = StringJoiner(",")
            for (item in basketInfoFullDTO.listBasketItemsDTO!!) {
                listOfIds.add(item.id.toString())
            }
            this.listBasketItemsIDs = listOfIds.toString()
            this.delivery = basketInfoFullDTO.delivery
            this.id = basketInfoFullDTO.delivery
            this.total = basketInfoFullDTO.total
        }

    fun mapBasketInfoFullDTOtoBasketItemsDBE(basketInfoFullDTO: BasketInfoFullDTO): List<BasketItemDBE> {
        val result = mutableListOf<BasketItemDBE>()

        for (basket in basketInfoFullDTO.listBasketItemsDTO!!) {
            val newItem = BasketItemDBE().apply {
                this.id = basket.id
                this.images = basket.images
                this.price = basket.price
                this.title = basket.title
            }
            result.add(newItem)
        }
        return result
    }

    fun mapBasketInfoFullDBEtoObject(basketInfoFullDBE: BasketInfoFullDBE): BasketInfoFull {
        return BasketInfoFull().apply {
            this.id = basketInfoFullDBE.id
            this.basketList = basketInfoFullDBE.listBasketItemsIDs.split(",")
            this.delivery = basketInfoFullDBE.delivery
            this.total = basketInfoFullDBE.total
        }
    }

    private fun mapBasketItemDBEtoObject(basketItemsDBE: BasketItemDBE): Basket {
        return Basket().apply {
            this.id = basketItemsDBE.id
            this.images = basketItemsDBE.images
            this.price = basketItemsDBE.price
            this.title = basketItemsDBE.title
            this.numberOfPhones = basketItemsDBE.numberOfPhones
            this.totalPrice = basketItemsDBE.totalPrice
        }
    }

    fun mapBasketItemDBEListtoListofObjects(list: List<BasketItemDBE>): List<Basket> {
        val result = mutableListOf<Basket>()
        for (basketDBE in list) {
            val item = mapBasketItemDBEtoObject(basketDBE)
            result.add(item)
        }

        return result
    }

}