package com.example.effectivemobiletestapp.data.db.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "hot_sales_phone_table")
data class HotSalesPhoneDBE(
    @PrimaryKey
    @NonNull
    var id: Int = 0,
    var isNew: Boolean = false,
    var title: String = "",
    var subtitle: String = "",
    var picture: String = "",
    var isBuy: Boolean = false
)