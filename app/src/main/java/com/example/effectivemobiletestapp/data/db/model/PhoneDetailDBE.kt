package com.example.effectivemobiletestapp.data.db.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "phone_detail_table")
data class PhoneDetailDBE(
    var cpu: String = "",
    var camera: String = "",
    var capacity: String = "",
    var color: String = "",
    @PrimaryKey
    @NonNull
    var id: String = "",
    var images: String = "",
    var isFavorites: Boolean = false,
    var price: Int = 0,
    var rating: Float = 0.0F,
    var sd: String = "",
    var ssd: String = "",
    var title: String = ""
)