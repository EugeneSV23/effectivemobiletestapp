package com.example.effectivemobiletestapp.data.db.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.effectivemobiletestapp.domain.model.Basket
import java.io.Serializable

@Entity(tableName = "basket_full_table")
data class BasketInfoFullDBE(
    var listBasketItemsIDs: String = "",
    var delivery: String = "",
    @PrimaryKey
    @NonNull
    var id: String = "",
    var total: Int = 0
)