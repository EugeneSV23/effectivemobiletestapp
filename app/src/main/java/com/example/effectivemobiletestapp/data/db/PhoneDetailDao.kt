package com.example.effectivemobiletestapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.effectivemobiletestapp.data.db.model.PhoneDetailDBE

@Dao
interface PhoneDetailDao {
    @Query(value = "SELECT * FROM phone_detail_table LIMIT 1")
    fun getPhoneDetail(): LiveData<PhoneDetailDBE?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPhoneDetail(phoneDetailDBE: PhoneDetailDBE)

    @Query("SELECT * FROM phone_detail_table WHERE id == :id LIMIT 1")
    suspend fun getPhoneDetailById(id: String): PhoneDetailDBE

    @Query("UPDATE phone_detail_table SET isFavorites = :isFavorite WHERE id = :id ")
    suspend fun setIsFavorite(isFavorite: Boolean, id: String)
}