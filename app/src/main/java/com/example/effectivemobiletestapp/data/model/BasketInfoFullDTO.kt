package com.example.effectivemobiletestapp.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class BasketInfoFullDTO {
    @SerializedName("basket")
    @Expose
    var listBasketItemsDTO: List<BasketItemsDTO>? = null

    @SerializedName("delivery")
    @Expose
    var delivery: String = ""

    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("total")
    @Expose
    var total: Int = 0
}