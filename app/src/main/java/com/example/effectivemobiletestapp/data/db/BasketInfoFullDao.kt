package com.example.effectivemobiletestapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.effectivemobiletestapp.data.db.model.BasketInfoFullDBE

@Dao
interface BasketInfoFullDao {
    @Query(value = "SELECT * FROM basket_full_table")
    fun getBasketInfoFull(): LiveData<BasketInfoFullDBE>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBasketInfoFull(BasketInfoFull: BasketInfoFullDBE)

}