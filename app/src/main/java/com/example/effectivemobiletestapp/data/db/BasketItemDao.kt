package com.example.effectivemobiletestapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.effectivemobiletestapp.data.db.model.BasketInfoFullDBE
import com.example.effectivemobiletestapp.data.db.model.BasketItemDBE
import com.example.effectivemobiletestapp.data.db.model.BestSellerPhoneDBE

@Dao
interface BasketItemDao {
    @Query(value = "SELECT * FROM basket_items_table")
    fun getBasketItemsList(): LiveData<List<BasketItemDBE>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBasketItemsList(List: List<BasketItemDBE>)

    @Query("UPDATE basket_items_table SET numberOfPhones = :numberOfPhones WHERE id = :id ")
    suspend fun changeNumberOfPhones(numberOfPhones: Int, id: Int)

    @Query("SELECT * FROM basket_items_table WHERE id = :id ")
    suspend fun getBasketItemDBE( id: Int) : BasketItemDBE

    @Query("UPDATE basket_items_table SET totalPrice = :totalPrice WHERE id = :id ")
    suspend fun updatePrice(totalPrice: Int, id: Int)

    @Query("DELETE FROM basket_items_table WHERE id = :id")
    suspend fun deleteBasketItem(id: Int)
}
