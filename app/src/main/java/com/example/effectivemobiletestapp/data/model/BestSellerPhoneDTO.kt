package com.example.effectivemobiletestapp.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class BestSellerPhoneDTO {
    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("is_favorites")
    @Expose
    var isFavorites: Boolean = false

    @SerializedName("title")
    @Expose
    var title: String = ""

    @SerializedName("price_without_discount")
    @Expose
    var priceWithoutDiscount: Int = 0

    @SerializedName("discount_price")
    @Expose
    var discountPrice: Int = 0

    @SerializedName("picture")
    @Expose
    var picture: String = ""
}