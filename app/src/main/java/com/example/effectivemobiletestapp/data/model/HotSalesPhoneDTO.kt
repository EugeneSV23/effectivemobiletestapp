package com.example.effectivemobiletestapp.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class HotSalesPhoneDTO {
    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("is_new")
    @Expose
    var isNew: Boolean = false

    @SerializedName("title")
    @Expose
    var title: String = ""

    @SerializedName("subtitle")
    @Expose
    var subtitle: String = ""

    @SerializedName("picture")
    @Expose
    var picture: String =""

    @SerializedName("is_buy")
    @Expose
    var isBuy: Boolean = false
}