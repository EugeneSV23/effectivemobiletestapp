package com.example.effectivemobiletestapp.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class PhoneResponseDTO {
    @SerializedName("home_store")
    @Expose
    var hotSales: List<HotSalesPhoneDTO>? = null

    @SerializedName("best_seller")
    @Expose
    var bestSeller: List<BestSellerPhoneDTO>? = null
}