package com.example.effectivemobiletestapp.data.network

import com.example.effectivemobiletestapp.data.mapper.Mapper
import com.example.effectivemobiletestapp.data.model.BasketInfoFullDTO
import com.example.effectivemobiletestapp.data.model.PhoneDetailDTO
import com.example.effectivemobiletestapp.data.model.PhoneResponseDTO
import com.example.effectivemobiletestapp.data.network.api.ApiService
import com.example.effectivemobiletestapp.domain.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RemoteData @Inject constructor(
    private val apiService: ApiService,
    @IoDispatcher
    private val ioDispatcher: CoroutineDispatcher
) {
    suspend fun loadHomePagePhoneData(): PhoneResponseDTO? {
        val homePagePhoneDataResponse =
            withContext(ioDispatcher) {
                apiService.getHomePagePhoneData()
            }
        return if (homePagePhoneDataResponse.isSuccessful && homePagePhoneDataResponse.body() != null) homePagePhoneDataResponse.body()
        else null
    }

    suspend fun loadDetailData(): PhoneDetailDTO? {
        val phoneDetailDataResponse =
            withContext(ioDispatcher) {
                apiService.getDetailPageData()
            }
        return if (phoneDetailDataResponse.isSuccessful && phoneDetailDataResponse.body() != null) phoneDetailDataResponse.body()
        else null
    }

    suspend fun loadBasketInfoData(): BasketInfoFullDTO? {
        val basketInfoDataResponse =
            withContext(ioDispatcher) {
                apiService.getBasketPageData()
            }
        return if (basketInfoDataResponse.isSuccessful && basketInfoDataResponse.body() != null) basketInfoDataResponse.body()
        else null
    }
}



