package com.example.effectivemobiletestapp.data.db

import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.effectivemobiletestapp.data.db.model.BestSellerPhoneDBE

@Dao
interface BestSellerPhoneDao {
    @Query(value = "SELECT * FROM best_seller_phone_table")
    fun getBestSellerPhoneList(): LiveData<List<BestSellerPhoneDBE>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBestSellerPhoneList(List: List<BestSellerPhoneDBE>)

    @Query("UPDATE best_seller_phone_table SET isFavorites = :isFavorite WHERE id = :id ")
    suspend fun setIsFavorite(isFavorite: Boolean, id: Int)
}
