package com.example.effectivemobiletestapp.data.db.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "best_seller_phone_table")
data class BestSellerPhoneDBE(
    @PrimaryKey
    @NonNull
    var id: Int = 0,
    var isFavorites: Boolean = false,
    var title: String = "",
    var priceWithoutDiscount: Int = 0,
    var discountPrice: Int = 0,
    var picture: String = ""
)