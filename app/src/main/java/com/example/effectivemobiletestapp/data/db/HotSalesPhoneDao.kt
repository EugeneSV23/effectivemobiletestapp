package com.example.effectivemobiletestapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.effectivemobiletestapp.data.db.model.HotSalesPhoneDBE

@Dao
interface HotSalesPhoneDao {
    @Query(value = "SELECT * FROM hot_sales_phone_table")
    fun getHotSalesPhoneDBEList(): LiveData<List<HotSalesPhoneDBE>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHotSalesPhoneDBEList(List: List<HotSalesPhoneDBE>)

    @Query("UPDATE hot_sales_phone_table SET isBuy = :isBuy WHERE id = :id ")
    suspend fun setIsBuy(isBuy: Boolean, id: Int)
}