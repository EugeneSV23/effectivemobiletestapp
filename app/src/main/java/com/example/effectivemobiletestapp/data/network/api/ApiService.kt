package com.example.effectivemobiletestapp.data.network.api

import com.example.effectivemobiletestapp.data.model.BasketInfoFullDTO
import com.example.effectivemobiletestapp.data.model.PhoneDetailDTO
import com.example.effectivemobiletestapp.data.model.PhoneResponseDTO
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET(value = "654bd15e-b121-49ba-a588-960956b15175")
    suspend fun getHomePagePhoneData(): Response<PhoneResponseDTO>

    @GET(value = "6c14c560-15c6-4248-b9d2-b4508df7d4f5")
    suspend fun getDetailPageData(): Response<PhoneDetailDTO>

    @GET(value = "53539a72-3c5f-4f30-bbb1-6ca10d42c149")
    suspend fun getBasketPageData(): Response<BasketInfoFullDTO>
}