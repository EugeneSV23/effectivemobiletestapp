package com.example.effectivemobiletestapp.data.db.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "basket_items_table")
data class BasketItemDBE(
    @PrimaryKey
    @NonNull
    var id: Int = 0,
    var images: String = "",
    var price: Int = 0,
    var title: String = "",
    var numberOfPhones: Int = 0,
    var totalPrice: Int = 0

)
