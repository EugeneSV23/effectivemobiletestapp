package com.example.effectivemobiletestapp.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class PhoneDetailDTO {
    @SerializedName("CPU")
    @Expose
    var cpu: String = ""

    @SerializedName("camera")
    @Expose
    var camera: String = ""

    @SerializedName("capacity")
    @Expose
    var capacity: List<String> = mutableListOf()

    @SerializedName("color")
    @Expose
    var color: List<String> = mutableListOf()

    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("images")
    @Expose
    var images: List<String> = mutableListOf()

    @SerializedName("isFavorites")
    @Expose
    var isFavorites: Boolean = false

    @SerializedName("price")
    @Expose
    var price: Int = 0

    @SerializedName("rating")
    @Expose
    var rating: Float = 0.0F

    @SerializedName("sd")
    @Expose
    var sd: String = ""

    @SerializedName("ssd")
    @Expose
    var ssd: String = ""

    @SerializedName("title")
    @Expose
    var title: String = ""
}