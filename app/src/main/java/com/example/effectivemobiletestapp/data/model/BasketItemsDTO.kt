package com.example.effectivemobiletestapp.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class BasketItemsDTO {
    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("images")
    @Expose
    var images: String = ""

    @SerializedName("price")
    @Expose
    var price: Int = 0

    @SerializedName("title")
    @Expose
    var title: String = ""
}