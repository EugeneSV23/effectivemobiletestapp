package com.example.effectivemobiletestapp.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.map
import com.example.effectivemobiletestapp.data.db.AppDatabase
import com.example.effectivemobiletestapp.data.mapper.Mapper
import com.example.effectivemobiletestapp.data.network.RemoteData
import com.example.effectivemobiletestapp.domain.model.BestSellerPhone
import com.example.effectivemobiletestapp.domain.model.HomePageTopMenu
import com.example.effectivemobiletestapp.domain.model.HotSalesPhone
import com.example.effectivemobiletestapp.domain.model.PhoneDetail
import com.example.effectivemobiletestapp.presentation.providers.UiDataProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repo @Inject constructor(
    private val remoteData: RemoteData,
    private val database: AppDatabase,
    private val mapper: Mapper
) {
    val bestSellerList: LiveData<List<BestSellerPhone>> =
        Transformations.map(database.bestSellerPhoneDao().getBestSellerPhoneList()) {
            mapper.mapBestSellerPhoneDBEListtoListofObjects(it)
        }

    suspend fun setIsFavoriteBestSeller(isFavorite: Boolean, id: Int) {
        database.bestSellerPhoneDao().setIsFavorite(isFavorite, id)
    }

    suspend fun insertBestSellerListinDB() {
        val bestSellerList = remoteData.loadHomePagePhoneData()?.bestSeller
        bestSellerList?.let {
            database.bestSellerPhoneDao()
                .insertBestSellerPhoneList(mapper.mapBestSellerPhoneDTOListtoDB(it))
        }
    }

    val hotSalesList: LiveData<List<HotSalesPhone>> =
        Transformations.map(database.hotSalesPhoneDao().getHotSalesPhoneDBEList()) {
            mapper.mapHotSalesPhoneDBEListtoListofObjects(it)
        }

    suspend fun setIsBuy(isBuy: Boolean, id: Int) {
        database.hotSalesPhoneDao().setIsBuy(isBuy, id)
    }

    suspend fun insertHotSalesListinDB() {
        val hotSalesList = remoteData.loadHomePagePhoneData()?.hotSales
        hotSalesList?.let {
            database.hotSalesPhoneDao()
                .insertHotSalesPhoneDBEList(mapper.mapHotSalesPhoneDTOlisttoDB(it))
        }
    }

    val phoneDetailLD =
        Transformations.map(database.phoneDetailDao().getPhoneDetail()) {
            mapper.mapPhoneDetailDBEtoObject(it)
        }

    val phoneDetImages =
        Transformations.map(phoneDetailLD) {
            mapper.mapPhoneDetImagesFromPhoneDetObj(it)
        }

    suspend fun setIsFavoritePhoneDet(isFavorite: Boolean, id: String) {
        database.phoneDetailDao().setIsFavorite(isFavorite, id)
    }

    suspend fun insertPhoneDetailinDB() {
        remoteData.loadDetailData()?.let { phoneDetailDTO ->
            mapper.mapPhoneDetailDTOtoDBE(phoneDetailDTO)
        }?.let {
            database.phoneDetailDao().insertPhoneDetail(it)
        }
    }

    suspend fun getPhoneDetailFromDB(id: String): PhoneDetail? {
        return mapper.mapPhoneDetailDBEtoObject(
            database.phoneDetailDao().getPhoneDetailById(id)
        )
    }

    val basketInfoFullLD = Transformations.map(database.basketInfoFullDao().getBasketInfoFull()) {
        mapper.mapBasketInfoFullDBEtoObject(it)
    }

    val basketItemsLD = database.basketItemDao().getBasketItemsList().map{
        mapper.mapBasketItemDBEListtoListofObjects(it)
    }

    suspend fun insertBasketInfoFullinDB() {
        remoteData.loadBasketInfoData()?.let { basketInfoFullDTO ->
            mapper.mapBasketInfoFullDTOtoDBE(basketInfoFullDTO)
        }?.let {
            database.basketInfoFullDao().insertBasketInfoFull(it)
        }
    }

    suspend fun insertBasketInfoItemsinDB() {
        remoteData.loadBasketInfoData()?.let { basketInfoItemsDTO ->
            mapper.mapBasketInfoFullDTOtoBasketItemsDBE(basketInfoItemsDTO)
        }?.let {
            database.basketItemDao().insertBasketItemsList(it)
        }
    }

    suspend fun changeNumberOfPhonesInDB(numberOfPhones: Int, id: Int) {
        database.basketItemDao().changeNumberOfPhones(numberOfPhones, id)
        val h = database.basketItemDao().getBasketItemDBE(id)

    }
    suspend fun updatePrice(price: Int, id: Int) {
        database.basketItemDao().updatePrice(price, id)
    }

    suspend fun deleteBasketItem(id: Int) {
        database.basketItemDao().deleteBasketItem(id)
    }

    val homePageMenuList = UiDataProvider.homeTopMenuItems

    fun setIsCheckedForMenuItem(item: HomePageTopMenu) {
        if (!item.isSelected) {
            homePageMenuList.value?.forEach {
                it.isSelected = it.id == item.id
            }
            homePageMenuList.value?.let {
                UiDataProvider.setHomeTopMenuItems(
                    it, UiDataProvider.homeTopMenuItems
                )
            }
        }
    }
}