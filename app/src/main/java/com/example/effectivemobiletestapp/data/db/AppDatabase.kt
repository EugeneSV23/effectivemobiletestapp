package com.example.effectivemobiletestapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.effectivemobiletestapp.data.db.model.*

@Database(
    entities = [BasketInfoFullDBE::class, BasketItemDBE::class, BestSellerPhoneDBE::class, HotSalesPhoneDBE::class, PhoneDetailDBE::class],
    version = 3,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun basketInfoFullDao(): BasketInfoFullDao
    abstract fun basketItemDao(): BasketItemDao
    abstract fun bestSellerPhoneDao(): BestSellerPhoneDao
    abstract fun hotSalesPhoneDao(): HotSalesPhoneDao
    abstract fun phoneDetailDao(): PhoneDetailDao
}