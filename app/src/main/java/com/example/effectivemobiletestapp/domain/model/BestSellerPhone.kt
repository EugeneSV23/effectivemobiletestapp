package com.example.effectivemobiletestapp.domain.model

data class BestSellerPhone(
    var id: Int = 0,
    var isFavorites: Boolean = false,
    var title: String = "",
    var priceWithoutDiscount: String = "",
    var discountPrice: String = "",
    var picture: String = ""
)