package com.example.effectivemobiletestapp.domain.interactor

import com.example.effectivemobiletestapp.data.repository.Repo
import javax.inject.Inject

class BasketInfoInteractor @Inject constructor(private val repository: Repo) {

    val basketInfoFullforUi = repository.basketInfoFullLD

    val basketItemsLDforUI = repository.basketItemsLD

    suspend fun loadAndInsertBasketInfoFullinDB() {
        repository.insertBasketInfoFullinDB()
    }
    suspend fun loadAndInsertBasketItemsinDB() {
        repository.insertBasketInfoItemsinDB()
    }

    suspend  fun changeNumberOfPhonesInDB(numberOfPhones: Int, id: Int) {
        repository.changeNumberOfPhonesInDB(numberOfPhones, id)
    }
    suspend fun updatePrice(price: Int, id: Int) {
        repository.updatePrice(price, id)
    }

    suspend fun deleteBasketItem(id: Int) {
        repository.deleteBasketItem(id)
    }
}
