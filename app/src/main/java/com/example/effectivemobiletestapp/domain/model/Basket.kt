package com.example.effectivemobiletestapp.domain.model

data class Basket (
    var id: Int = 0,
    var images: String = "",
    var price: Int = 0,
    var title: String = "",
    var numberOfPhones: Int = 1,
    var totalPrice: Int = 0
)