package com.example.effectivemobiletestapp.domain.model

data class BasketInfoFull(
    var basketList: List<String>? = null,
    var delivery: String = "",
    var id: String = "",
    var total: Int = 0
)