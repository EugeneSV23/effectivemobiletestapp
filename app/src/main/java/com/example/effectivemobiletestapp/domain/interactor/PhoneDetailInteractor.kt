package com.example.effectivemobiletestapp.domain.interactor

import com.example.effectivemobiletestapp.data.repository.Repo
import com.example.effectivemobiletestapp.domain.model.PhoneDetail
import javax.inject.Inject

class PhoneDetailInteractor @Inject constructor(private val repository: Repo) {

    val phoneDetailforUi = repository.phoneDetailLD

    val phoneDetImages = repository.phoneDetImages

    suspend fun loadAndInsertphoneDetailinDB() {
        repository.insertPhoneDetailinDB()
    }

    suspend fun setIsFavoritePhoneDet(isFavorite: Boolean, id: String) {
        repository.setIsFavoritePhoneDet(isFavorite, id)
    }

   suspend fun getPhoneDetailFromDB(id: String): PhoneDetail? {
        return repository.getPhoneDetailFromDB(id)
    }
}
