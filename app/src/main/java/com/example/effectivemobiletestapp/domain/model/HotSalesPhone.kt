package com.example.effectivemobiletestapp.domain.model

data class HotSalesPhone(
    var id: Int = 0,
    var isNew: Boolean = false,
    var title: String = "",
    var subtitle: String = "",
    var picture: String = "",
    var isBuy: Boolean = false
)