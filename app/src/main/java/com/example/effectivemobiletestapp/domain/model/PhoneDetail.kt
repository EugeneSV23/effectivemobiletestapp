package com.example.effectivemobiletestapp.domain.model

data class PhoneDetail(
    var cpu: String = "",
    var camera: String = "",
    var capacity: List<String> = mutableListOf(),
    var color: List<String> = mutableListOf(),
    var id: String = "",
    var images: List<String> = mutableListOf(),
    var isFavorites: Boolean = false,
    var price: String = "",
    var rating: Float = 0.0F,
    var sd: String = "",
    var ssd: String = "",
    var title: String = ""
)