package com.example.effectivemobiletestapp.domain.di

import android.content.Context
import androidx.room.Room
import com.example.effectivemobiletestapp.data.db.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    fun provideBasketInfoFullDao(database: AppDatabase): BasketInfoFullDao {
        return database.basketInfoFullDao()
    }
    @Provides
    fun provideBestSellerPhoneDao(database: AppDatabase): BestSellerPhoneDao {
        return database.bestSellerPhoneDao()
    }
    @Provides
    fun provideHomeStorePhoneDao(database: AppDatabase): HotSalesPhoneDao {
        return database.hotSalesPhoneDao()
    }
    @Provides
    fun providePhoneDetailDao(database: AppDatabase): PhoneDetailDao {
        return database.phoneDetailDao()
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "phones.db"
        ).fallbackToDestructiveMigration().build()
    }
}