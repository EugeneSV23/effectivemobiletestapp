package com.example.effectivemobiletestapp.domain.interactor

import com.example.effectivemobiletestapp.data.repository.Repo
import com.example.effectivemobiletestapp.domain.model.HomePageTopMenu
import javax.inject.Inject


class HomePhoneInteractor @Inject constructor(private val repository: Repo) {

    val hotSalesListForUi = repository.hotSalesList

    val bestSellerListForUi = repository.bestSellerList

    val homePageMenuList = repository.homePageMenuList


    suspend fun loadAndInsertHotSalesListinDB() {
        repository.insertHotSalesListinDB()
    }

    suspend fun loadAndInsertBestSellerListinDB() {
        repository.insertBestSellerListinDB()
    }

    suspend fun setIsFavoriteBestSeller(isFavorite: Boolean, id: Int){
        repository.setIsFavoriteBestSeller(isFavorite, id)
    }

    suspend fun setIsBuy(isBuy: Boolean, id: Int){
        repository.setIsBuy(isBuy, id)
    }

    fun setIsCheckedForMenuItem(item: HomePageTopMenu) {
       repository.setIsCheckedForMenuItem(item)
    }
}