package com.example.effectivemobiletestapp.domain.model

data class HomePageTopMenu(
    var id: Int = 0,
    var picture: Int = 0,
    var isSelected: Boolean = false,
    var title: String = ""
)

