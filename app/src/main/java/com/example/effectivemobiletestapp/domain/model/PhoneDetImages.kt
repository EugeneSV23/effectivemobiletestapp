package com.example.effectivemobiletestapp.domain.model

data class PhoneDetImages(
    var image: String = ""
)